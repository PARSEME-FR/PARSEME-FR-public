ANR PARSEME-FR
==============

Public gitlab of the [PARSEME-FR ANR project](http://parsemefr.lis-lab.fr). 


This repository contains publicly released resources. These resources include:

* [Annotation guidelines](https://gitlab.lis-lab.fr/PARSEME-FR/PARSEME-FR-public/wikis/home)
* Corpora, annotated corpora, treebanks
* Lexicons